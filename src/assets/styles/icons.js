import { createGlobalStyle } from 'styled-components';

const icomoonEot = '/static/fonts/icon/icomoon.eot';
const icomoonTtf = '/static/fonts/icon/icomoon.ttf';
const icomoonWoff = '/static/fonts/icon/icomoon.woff';
const icomoonSvg = '/static/fonts/icon/icomoon.svg';

const icons = createGlobalStyle`
  @font-face {
    font-family: 'icomoon';
    src:  url(${icomoonEot}?4ppcpr);
    src:  url(${icomoonEot}?4ppcpr#iefix) format('embedded-opentype'),
      url(${icomoonTtf}?4ppcpr) format('truetype'),
      url(${icomoonWoff}?4ppcpr) format('woff'),
      url(${icomoonSvg}?4ppcpr#icomoon) format('svg');
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }

  [class^="icon-"], [class*=" icon-"] {
    /* use !important to prevent issues with browser extensions that change fonts */
    font-family: 'icomoon' !important;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .icon-instagram:before {
    content: "\\e90b";
    color: #e4405f;
  }
  .icon-linkedin:before {
    content: "\\e907";
    color: #0077b5;
  }
  .icon-twitter:before {
    content: "\\e908";
    color: #1da1f2;
  }
  .icon-youtube:before {
    content: "\\e909";
    color: #f00;
  }
  .icon-facebook:before {
    content: "\\e90a";
    color: #1877f2;
  }
  .icon-menu:before {
    content: "\\e906";
  }
  .icon-edit:before {
    content: "\\e905";
  }
  .icon-trash-2:before {
    content: "\\e900";
  }
  .icon-chevron-left:before {
    content: "\\e901";
  }
  .icon-cancel-circle:before {
    content: "\\e902";
  }
  .icon-close:before {
    content: "\\e902";
  }
  .icon-remove:before {
    content: "\\e902";
  }
  .icon-delete:before {
    content: "\\e902";
  }
  .icon-eye-blocked:before {
    content: "\\e903";
  }
  .icon-views:before {
    content: "\\e903";
  }
  .icon-vision:before {
    content: "\\e903";
  }
  .icon-visit:before {
    content: "\\e903";
  }
  .icon-banned:before {
    content: "\\e903";
  }
  .icon-blocked:before {
    content: "\\e903";
  }
  .icon-forbidden:before {
    content: "\\e903";
  }
  .icon-private:before {
    content: "\\e903";
  }
  .icon-eye:before {
    content: "\\e904";
  }
  .icon-views1:before {
    content: "\\e904";
  }
  .icon-vision1:before {
    content: "\\e904";
  }
  .icon-visit1:before {
    content: "\\e904";
  }

`

export default icons;