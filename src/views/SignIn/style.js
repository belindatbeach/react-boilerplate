import styled from 'styled-components';
import { Primary } from '@assets/styles/colors';

export const PageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 90vh;
  padding: 20px;
`;

export const PageWrapper = styled.div`
  border: 1px solid ${Primary};
  padding: 40px 32px;
  width: 485px;
  box-shadow: 0px 0 5px rgb(1 41 112 / 8%);
  border-radius: 10px;
`;

export const LogoWrapper = styled.div`
  width: 236px;
  margin: 0 auto 40px;
  pointer-events: none;
`;

export const Form = styled.form`
  width: 100%;
  @media (min-width: 768px) {
		width: 100%;
	}
`;