
import React from 'react'
import Page from '@components/templates/Page';
import Info from '@components/Info';
import { homeObjOne, homeObjThree, homeObjTwo } from '@components/Info/Data';
import Services from '@components/Services';
import Footer from '@components/Footer';
import Hero from '@components/Hero';

function Landing() {
    return (
        <Page title="Home" description="Home" showNav={false} layout="home">
            <Hero />
            <Info {...homeObjOne} />
			<Info {...homeObjTwo} />
			<Services />
			<Info {...homeObjThree} />
            <Footer />
        </Page>
    )
}

export default Landing
