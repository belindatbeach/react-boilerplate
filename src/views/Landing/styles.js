import { HeadingLarge, ParagraphExtraLarge } from '@assets/styles/typography';
import styled from 'styled-components';

export const PageWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #0c0c0c;
    padding: 0 30px;
    height: 90vh;
    position: relative;
    z-index: 1;
    :before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.6);
        z-index: 1;
    }
`;

export const VideoBg = styled.video`
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 100%;
	object-fit: cover;
	background-color: #232a34;
`;

export const HeroContent = styled.div`
	z-index: 3;
`;

export const HeroH1 = styled.h1`
	color: var(--primaryWhite);    
    ${HeadingLarge};
    @media screen and (max-width: 768px) {
        text-align: center;
    }
`;

export const HeroP = styled.p`
	margin-top: 20px;
	color: #fff;
    ${ParagraphExtraLarge};
    max-width: 600px;
    @media screen and (max-width: 768px) {
        text-align: center;
    }
`;
export const LostPicture = styled.img`
  
`;