
import React from 'react'
import Page from '@components/templates/Page';
import Trade from '@components/_Trade'
function Trading() {
    return (
        <Page title="Home" description="Home" showNav={false} layout="account">
            <Trade />
        </Page>
    )
}

export default Trading
