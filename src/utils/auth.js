import cookie from 'js-cookie';
import jwt_decode from 'jwt-decode';
import AuthService from '@api/services/auth';

const tokenName = `${process.env.REACT_APP_PROJECT_NAME}-token`;

const ACCESS_TOKEN = `${process.env.REACT_APP_PROJECT_NAME}-token`;
const REFRESH_TOKEN = `${process.env.REACT_APP_PROJECT_NAME}-refreshToken`;

export const isAuthenticated = () => {
  return !!cookie.get(tokenName);
}

export const signin = async ({ email, password }) => {
  const { data } = await AuthService.signin({
    email,
    password,
  });

  cookie.set(tokenName, data.token, { expires: 1 });
};

export const logout = () => {
  cookie.remove(tokenName);
  window.location.reload();
};

export const getAccessToken = () => {
  const accessToken = cookie.get(ACCESS_TOKEN);
  if (!accessToken || accessToken === 'null') {
     return null;
  }
  return expiredToken(accessToken) ? null : accessToken;
};

export const getRefreshToken = () => {
  const refreshToken = cookie.get(REFRESH_TOKEN);
  if (!refreshToken || refreshToken === 'null') {
     return null;
  }
  return expiredToken(refreshToken) ? null : refreshToken;
};

const expiredToken = (token) => {
  const seconds = 60;
  const metaToken = jwt_decode(token);
  const { exp } = metaToken;
  const now = (Date.now() + seconds) / 1000;
  return now > exp;
};
