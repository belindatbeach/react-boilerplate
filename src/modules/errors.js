export const errorMessages = {
  email: {
    required: 'Required email',
    pattern: 'Invalid email',
  },
  password: {
    required: 'Required password',
  },
  birthdate: {
    pattern: 'Invalid date',
  },
  admission_date: {
    pattern: 'Invalid date',
  },
  name: {
    required: 'Name Required',
  },
  defaults: {
    required: 'Required field',
  },
};

export const requestErrorMessages = {
  400: 'Bad Request. Your browser sent a request that this server could not understand.',
  401: 'Authorization Required',
  500: 'Internal Server Error',
};

export function getErrorMessage(error) {
  const { type, ref } = error;
  const { name } = ref;
  let errorMessage = '';

  if (errorMessages[name] && errorMessages[name][type]) errorMessage = errorMessages[name][type];
  else if (errorMessages.defaults[type]) errorMessage = errorMessages.defaults[type];
  else errorMessage = 'field error';

  return errorMessage;
};

export function getErrorMessageByRequest(error) {
  const { response } = error;
  if (!response) return false;
  const { status, data } = response;
  const { message } = data;

  if (message) return message;

  return requestErrorMessages[status];
}
