import React from 'react'
import Button from '@components/common/Button';
import video from "@assets/video.mp4";
import { PageWrapper, VideoBg, HeroContent, HeroH1, HeroP } from './styles';
const Hero = () => {
    return (
        <PageWrapper>
            <VideoBg autoPlay loop muted src={video} type='video/mp4' />
            <HeroContent className="container d-flex flex-column align-items-md-start justify-content-center w-100 py-3 px-5 ">
                <HeroH1 data-aos='fade-up' data-aos-duration='3000'>Virtual Banking Mode Easy</HeroH1>
                <HeroP data-aos='fade-up' data-aos-duration='1000'>
                    Sing Up For a new tody and receive $250 in credit towards your next
                    payment.
                    </HeroP>
                <div className="d-flex mt-3 justify-content-center justify-content-md-start w-100">
                    <Button width="150px" data-aos='fade-up' data-aos-duration='2400'>Adicionar Naver</Button>
                </div>
            </HeroContent>
        </PageWrapper>
    )
}

export default Hero