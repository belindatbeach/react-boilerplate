import React from "react";
import Icon1 from "@assets/img3.svg";
import Icon2 from "@assets/img4.svg";
import Icon3 from "@assets/img2.svg";
import {
    ServicesContainer,
    ServicesH1,
    ServicesWrapper,
    ServicesCardBox,
    ServicesCard,
    ServciesIcon,
    ServciesH2,
    ServicesP,
} from "./styles";

const Services = () => {
    return (
        <ServicesContainer id="services">
            <ServicesH1>Our Services</ServicesH1>
            <ServicesWrapper className="row">
                <ServicesCardBox className="col-lg-3 col-md-6" data-aos='fade-up' data-aos-duration='1000'>
                    <ServicesCard>
                        <ServciesIcon src={Icon1} />
                        <ServciesH2>Reduce expenses</ServciesH2>
                        <ServicesP>
                            We helo reduce your fess and increse your overall revenue
                        </ServicesP>
                    </ServicesCard>
                </ServicesCardBox>
                <ServicesCardBox className="col-lg-3 col-md-6 mt-md-0 mt-3" data-aos='fade-up' data-aos-duration='1500'>
                    <ServicesCard>
                        <ServciesIcon src={Icon2} />
                        <ServciesH2>Virtual Offices</ServciesH2>
                        <ServicesP>
                            You can access our platform online anywhere in the world!
					</ServicesP>
                    </ServicesCard>
                </ServicesCardBox>
                <ServicesCardBox className="col-lg-3 col-md-6 mt-lg-0 mt-3" data-aos='fade-up' data-aos-duration='2000'>
                    <ServicesCard>
                        <ServciesIcon src={Icon3} />
                        <ServciesH2>Premium Benefits</ServciesH2>
                        <ServicesP>
                            Unlock our special membership card that return 5% cash back.
					</ServicesP>
                    </ServicesCard>
                </ServicesCardBox>
                <ServicesCardBox className="col-lg-3 col-md-6 mt-lg-0 mt-3" data-aos='fade-up' data-aos-duration='2500'>
                    <ServicesCard>
                        <ServciesIcon src={Icon3} />
                        <ServciesH2>Premium Benefits</ServciesH2>
                        <ServicesP>
                            Unlock our special membership card that return 5% cash back.
					</ServicesP>
                    </ServicesCard>
                </ServicesCardBox>
            </ServicesWrapper>
        </ServicesContainer>
    );
};

export default Services;