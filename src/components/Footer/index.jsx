import React from 'react'
import {
	FooterContainer,
	FooterWrap,
	FooterLinksContainer,
	FooterLinksWrapper,
	FooterLinkItems,
	FooterLinkTitle,
	FooterLink,
	SocialMedia,
	SocialMediaWrap,
	WebsiteRight,
	SocialLogo,
	SocialIcons,
	SocialIconLink,
} from "./styles";
import { animateScroll as scroll } from "react-scroll";
import Icon from "@components/common/Icon";

const Footer = () => {
	const scrollTop = () => {
		scroll.scrollToTop();
	};
	return (
		<FooterContainer>
			<FooterWrap>
				<FooterLinksContainer>
					<FooterLinksWrapper>
						<FooterLinkItems>
							<FooterLinkTitle>About us1</FooterLinkTitle>
							<FooterLink to="/singin">How it works</FooterLink>
							<FooterLink to="/singin">Testimonials</FooterLink>
							<FooterLink to="/singin">Career</FooterLink>
							<FooterLink to="/singin">Investors</FooterLink>
							<FooterLink to="/singin">Terms Of Services</FooterLink>
						</FooterLinkItems>
						<FooterLinkItems>
							<FooterLinkTitle>About us2</FooterLinkTitle>
							<FooterLink to="/singin">How it works</FooterLink>
							<FooterLink to="/singin">Testimonials</FooterLink>
							<FooterLink to="/singin">Career</FooterLink>
							<FooterLink to="/singin">Investors</FooterLink>
							<FooterLink to="/singin">Terms Of Services</FooterLink>
						</FooterLinkItems>
					</FooterLinksWrapper>
					<FooterLinksWrapper>
						<FooterLinkItems>
							<FooterLinkTitle>About us3</FooterLinkTitle>
							<FooterLink to="/singin">How it works</FooterLink>
							<FooterLink to="/singin">Testimonials</FooterLink>
							<FooterLink to="/singin">Career</FooterLink>
							<FooterLink to="/singin">Investors</FooterLink>
							<FooterLink to="/singin">Terms Of Services</FooterLink>
						</FooterLinkItems>
						<FooterLinkItems>
							<FooterLinkTitle>About us4</FooterLinkTitle>
							<FooterLink to="/singin">How it works</FooterLink>
							<FooterLink to="/singin">Testimonials</FooterLink>
							<FooterLink to="/singin">Career</FooterLink>
							<FooterLink to="/singin">Investors</FooterLink>
							<FooterLink to="/singin">Terms Of Services</FooterLink>
						</FooterLinkItems>
					</FooterLinksWrapper>
				</FooterLinksContainer>
				<SocialMedia>
					<SocialMediaWrap>
						<SocialLogo to="/" onClick={scrollTop}>
							dolla
						</SocialLogo>
						<WebsiteRight>
							dolla &copy; {new Date().getFullYear()} All rights reserved.
						</WebsiteRight>
						<SocialIcons>
							<SocialIconLink href="/">
                                <Icon name="facebook" />
							</SocialIconLink>
							<SocialIconLink href="/">
                                <Icon name="instagram" />
							</SocialIconLink>
							<SocialIconLink href="/">
                                <Icon name="twitter" />
							</SocialIconLink>
							<SocialIconLink href="/">
                                <Icon name="youtube" />
							</SocialIconLink>
							<SocialIconLink href="/">
                                <Icon name="linkedin" />
							</SocialIconLink>
						</SocialIcons>
					</SocialMediaWrap>
				</SocialMedia>
			</FooterWrap>
		</FooterContainer>
	);
};

export default Footer;