import styled from 'styled-components'
export const Box = styled.div`
    box-shadow: 0 0 3px 3px rgba(0,0,0,0.2);
    padding: 5px;
    background-color: #3b3a48;
    background-image: linear-gradient(130deg, #3b3a48 0%, #212121 85%, #212121 100%);
    color: #fff;
    border: 5px solid #171717;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: ${({center}) => center ? 'center' : 'flex-start'};
`;

export const TradeGrid = styled.div`
    box-sizing: border-box;
    margin: 0px;
    margin-top: 1px;
    min-width: 0px;
    background-color: rgb(238, 240, 242);
    display: grid;
    gap: 1px;
    height: 150vh;
    
    grid-template-areas:
        "marketInfo marketInfo marketInfo market market"
        "orderbook orderbook chart market market"
        "orderbook orderbook chart market market"
        "orderbook orderbook chart trades trades"
        "orderbook orderbook orderform trades trades"
        "orderbook orderbook orderform trades trades"
        "orderHistory orderHistory orderHistory orderHistory orderHistory";
    @media screen and (min-width: 992px) {
        grid-template-columns: 4fr minmax(253px, 320px) minmax(510px, 880px) minmax(253px, 320px) 1fr;
        grid-template-rows: minmax(68px, auto) 68px 220px 160px 320px auto 285px;
    }
    @media screen and (max-width: 992px) {
        grid-template-columns: 1fr 1fr 1fr;
        grid-template-rows: minmax(64px, auto) minmax(64px, auto) 1.6fr 1.15fr 1fr;
        grid-template-areas:
            "marketInfo marketInfo marketInfo"
            "marketInfo marketInfo marketInfo"
            "chart chart orderform"
            "trades orderbook orderform"
            "orderHistory orderHistory market";
    }
    @media screen and (max-width: 460px) {
        grid-template-columns: 1fr 1fr; 
        grid-template-rows: 100px auto;
        grid-template-areas:
            "marketInfo marketInfo"
            "chart chart"
            "orderform orderform"
            "trades orderbook"
            "orderHistory orderHistory"
            "market market";
    }
`;
