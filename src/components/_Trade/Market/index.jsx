import React from 'react'
import { Box } from '../styles'
import { MarketWrap } from './styles'

function Market() {
    return (
        <MarketWrap>
            <Box>
            Market
            </Box>
        </MarketWrap>
    )
}

export default Market
