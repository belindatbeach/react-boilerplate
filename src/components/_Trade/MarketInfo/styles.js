import styled, {css} from 'styled-components';

export const MarketInfoWrap = styled.div.attrs({ name: 'marketInfo' })`
box-sizing: border-box;
margin: 0px;
min-width: 0px;
width: 100%;
height: 100%;
grid-area: marketInfo / marketInfo / marketInfo / marketInfo;
z-index: 10;
`;


export const TradeHeader = styled.div`
  position: relative;
  display: flex;
  flex-shrink: 0;
  flex-direction: row;
  height: 46px;
  background: ${p => p.theme.headerBgColor};
`

export const Text = styled.span`
  font-weight: bold;
  line-height: 1.5;
  word-break: normal;
  display: inline;
  font-family: atlas, opensans, sans-serif, btcglyph;
  font-size: 14px;
  text-align: left;
  text-transform: inherit;
  color:  ${p => p.theme.color};
`

export const Span = styled.span`
  font-weight: normal;
  line-height: 1.5;
  word-break: normal;
  display: inline;
  font-family: atlas, opensans, sans-serif, btcglyph;
  font-size: 11px;
  text-align: left;
  text-transform: inherit;
  color:  ${p => p.theme.color};
`


export const Section = styled.div`
  display: flex;
  flex-shrink: 0;
  height: 46px;
  margin-right: 30px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`

export const TradeCurrency = styled.div`
  display: flex;
  justify-content: space-between;
  cursor: pointer;
  height: 46px;
  position: relative;
  color: ${p => p.theme.color};
  width: 223px;
  border-right: 1px solid ${p => p.theme.border};
  padding: 0px 28px;
`
export const CurrentPrice = styled.div`
  display: flex;
  align-items: center;
  overflow: hidden;
  padding: 0px 28px;
`

export const Small = styled.small`
  font-weight: normal;
  line-height: 1.5;
  word-break: normal;
  display: inline;
  margin-left: 9px;
  font-family: atlas, opensans, sans-serif, btcglyph;
  font-size: 11px;
  text-align: left;
  text-transform: inherit;
  color: rgba(255, 255, 255, 0.4);
`
export const Grid = styled.div`
  display: flex;
  flex-shrink: 0;
  -webkit-box-pack: justify;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
export const Col = styled('div')`
  display: flex;
  flex-direction: row;
  ${(props) => props.empty && css`
      flex: 1 1 auto;
  `}
`;

export const ColSpace = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1 1 auto;
`;