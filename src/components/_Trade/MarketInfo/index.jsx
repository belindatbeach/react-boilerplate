import React from 'react'
import { Box } from '../styles'
import { Col, CurrentPrice, Grid, MarketInfoWrap, Section, Small, Span, Text, TradeCurrency, TradeHeader } from './styles'
function MarketInfo() {
    return (
        <MarketInfoWrap>
            <Box center='true'>
                <TradeHeader>
                    <Grid>
                        <Col>
                            <TradeCurrency>
                                <Section><Text>BTC-EUR</Text></Section>
                                <Section><Span>Select Market  </Span></Section>
                            </TradeCurrency>
                        </Col>
                        <Col>
                            <CurrentPrice>
                                <Section><Text>3,919.87 EUR</Text><Small>Last trade price</Small></Section>
                                <Section><Text>-4.52%</Text><Small>24h price</Small></Section>
                                <Section><Text>5,417 BTC</Text><Small>24h volume</Small></Section>
                            </CurrentPrice>
                        </Col>
                        <Col empty></Col>
                    </Grid>
                </TradeHeader>
            </Box>
        </MarketInfoWrap>
    )
}

export default MarketInfo
