import React from 'react'
import { Box } from '../styles';
import { OrderBookWrapper } from './styles';

function OrderBook() {
    return (
        <OrderBookWrapper>
            <Box>
                Order Book
            </Box>
        </OrderBookWrapper>
    )
}

export default OrderBook
