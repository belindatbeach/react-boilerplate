import styled from 'styled-components'
export const OrderBookWrapper = styled.div.attrs({ name: 'orderbook' })`
box-sizing: border-box;
margin: 0px;
min-width: 0px;
width: 100%;
height: 100%;
background-color: rgb(250, 250, 250);
grid-area: orderbook / orderbook / orderbook / orderbook;
position: relative;
`;