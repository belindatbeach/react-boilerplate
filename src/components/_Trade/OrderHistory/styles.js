import styled from 'styled-components';

export const OrderHistoryWrap = styled.div.attrs({ name: 'orderHistory' })`
    box-sizing: border-box;
    margin: 0px;
    min-width: 0px;
    width: 100%;
    height: 100%;
    background-color: rgb(250, 250, 250);
    grid-area: orderHistory / orderHistory / orderHistory / orderHistory;
`;