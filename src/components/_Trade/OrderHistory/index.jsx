import React from 'react'
import { Box } from '../styles'
import { OrderHistoryWrap } from './styles'

function OrderHistory() {
    return (
        <OrderHistoryWrap>
            <Box>
                Order History
            </Box>
        </OrderHistoryWrap>
    )
}

export default OrderHistory
