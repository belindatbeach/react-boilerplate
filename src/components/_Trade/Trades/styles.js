import styled from 'styled-components';

export const TradesWrap = styled.div.attrs({ name: 'trades' })`
    box-sizing: border-box;
    margin: 0px;
    min-width: 0px;
    width: 100%;
    height: 100%;
    grid-area: trades / trades / trades / trades;
    background-color: rgb(250, 250, 250);
    z-index: auto;
    position: relative;
`;