import React from 'react'
import { Box } from '../styles'
import { TradesWrap } from './styles'

function Trades() {
    return (
        <TradesWrap>
            <Box>
            Trades
            </Box>
        </TradesWrap>
    )
}

export default Trades
