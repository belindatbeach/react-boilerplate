import React from 'react'
import { Box } from '../styles'
import {ChartContainer} from './styles'

function Chart() {
    return (
        <ChartContainer>
            <Box>
                Chart
            </Box>
        </ChartContainer>
    )
}

export default Chart
