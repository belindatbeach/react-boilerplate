import React, { Component } from 'react'
import styled, { css } from 'styled-components';
import Chart from './Chart';
import Market from './Market';
import MarketInfo from './MarketInfo';
import OrderBook from './OrderBook';
import OrderForm from './OrderForm';
import OrderHistory from './OrderHistory';
import { Box, TradeGrid } from './styles';
import Trades from './Trades';


class Trade extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TradeGrid>
                <MarketInfo />
                <OrderBook />
                <Chart />
                <OrderForm />
                <Trades />
                <Market />
                <OrderHistory />
            </TradeGrid>
        )
    }
}







export default Trade;