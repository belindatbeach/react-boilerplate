import styled from 'styled-components';

export const OrderFormWrap = styled.div.attrs({ name: 'orderform' })`
box-sizing: border-box;
margin: 0px;
min-width: 0px;
grid-area: orderform / orderform / orderform / orderform;
background-color: rgb(255, 255, 255);
`;