import React from 'react'
import { Box } from '../styles'
import { OrderFormWrap } from './styles'

function OrderForm() {
    return (
        <OrderFormWrap>
            <Box>
                Order Form
            </Box>
        </OrderFormWrap>
    )
}

export default OrderForm
