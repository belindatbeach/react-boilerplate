import Button from "@components/common/Button";
import React from "react";
import {
	InfoContainer,
	InfoWrapper,
	Row,
	Column,
	TextWrapper,
	TopLine,
	Heading,
	SubTitle,
	BtnWrap,
	ImgWrap,
	Img,
} from "./styles";

const Info = ({
	id,
	lightBg,
	imgStart,
	topLine,
	lightText,
	headLine,
	darkText,
	description,
	buttonLabel,
	img,
	alt,
	primary,
	dark,
}) => {
	return (
		<InfoContainer lightBg={lightBg} id={id}>
			<InfoWrapper>
				<Row imgStart={imgStart}>
					<Column data-aos='fade-up'data-aos-duration='2400'>
						<TextWrapper>
							<TopLine>{topLine}</TopLine>
							<Heading lightText={lightText}>{headLine}</Heading>
							<SubTitle darkText={darkText}>{description}</SubTitle>
							<BtnWrap>
								<Button width="150px">
									{buttonLabel}
								</Button>
							</BtnWrap>
						</TextWrapper>
					</Column>
					<Column data-aos={imgStart ? 'fade-right': 'fade-left'} data-aos-delay='50' data-aos-duration='1000'>
						<ImgWrap>
							<Img src={img} alt={alt} />
						</ImgWrap>
					</Column>
				</Row>
			</InfoWrapper>
		</InfoContainer>
	);
};

export default Info;