import { ParagraphLarge } from "@assets/styles/typography";
import styled from "styled-components";

export const HeaderContainer = styled.div`
  /*position: sticky;*/
  top: 0;
  z-index: 999999;
  background-color: var(--primaryColor);
  height: 70px;
  display: flex;
  align-items: center;
  width: 100%;
  @media (max-width: 960px) {
        transition: 0.8s all ease;
    }
`;

export const HeaderNav = styled.div`
  display: flex;
  max-width: 1920px;
  margin: 0 auto;
  padding: 0 24px;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
`;

export const Logo = styled.img`
  height: 50px;
  width: 50px;
  object-fit: contain;
  padding-right: 10px;
`;

export const NavBars = styled.div`
	color: var(--primaryWhite);
	font-size: 1.5rem;
    cursor: pointer;
    display: flex;
    align-items: center;
	@media (min-width: 768px) {
		display: none;
	}
`;

export const HeaderRight = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 768px) {
    display: none;
}
`;

export const NavMenu = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-right: 30px;
  position: relative;
  cursor: pointer;
  span {
    ${ParagraphLarge};
    font-weight: 400;
    color: var(--primaryGrey);
    text-transform: capitalize;
    transition: all 0.25s ease;
    &:hover {
        color: var(--primaryWhite);
    }
  }
`;

export const User = styled.div`
  position: relative;
  padding-right: 30px;
  border-right: 0.5px solid lightgrey;
`;

export const Usertitle = styled.div`
  display: flex;
  align-items: center;
`;


export const UserDropDown = styled.div`
  position: absolute;
  z-index: 599999;
  top: 50px;
  right: 30px;
  border: none;
  background: var(--primaryColor);
  border-radius: 0 0 5px 5px;
  width: 264px;
  box-shadow: 3px 5px 10px 0px rgba(41, 50, 50, 0.37);
  button {
    width: 90%;
    margin-top: 20px;
    color: #0a66c2;
    border-color: #0a66c2;
    border-radius: 50px;
    background: transparent;
    padding: 2px 12px;
    text-transform: capitalize;
    transition: all 0.2s ease-in-out;
    margin-left: 10px;
    margin-right: 10px;
    &:hover {
      background-color: #c6e5f0;
      box-shadow: rgb(10, 102, 194) 0 0 0 1px;
    }
  }
  hr {
    border: 0.5px solid whitesmoke;
    margin-top: 10px;
  }
`;

export const UserDetails = styled.div`
  display: flex;
  align-items: center;
  img {
    height: 56px;
    width: 56px;
    object-fit: contain;
    border-radius: 50px;
    padding-right: 10px;
  }
`;

export const Userdesc = styled.div`
    margin-left: 10px;
`;

export const UserName = styled.h3`
  text-transform: uppercase;
  color: var(--primaryGrey);
`;

export const UseDescription = styled.h3`
  font-weight: 400;
  font-size: 16px;
  color: var(--primaryGrey);
`;

export const UserDropContent = styled.div`
  padding-left: 10px;
  padding-top: 5px;
`;

export const ContentHeading = styled.h3`
  padding-bottom: 8px;
  text-transform: capitalize;
  color: var(--primaryWhite);
`;

export const ContentList = styled.li`
  list-style: none;
  font-weight: 300;
  color: var(--primaryGrey);
  font-size: 15px;
  text-transform: capitalize;
  padding-bottom: 8px;
  &:hover {
    text-decoration: underline;
  }
`;

export const Work = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
export const Worktitle = styled.div`
  display: flex;
`;






