
import React, { useContext, useEffect, useRef, useState } from "react";
import Icon from "../Icon";
import {
    HeaderContainer,
    HeaderNav,
    HeaderLeft,
    Logo,
    NavBars,
    HeaderRight,
    NavMenu,
    User,
    UserImg,
    Usertitle,
    UserDropDown,
    UserDetails,
    Userdesc,
    UserName,
    UseDescription,
    UserDropContent,
    ContentHeading,
    ContentList,
    Work,
    Worktitle
} from "./styles";
import AppContext from "@context/appContext";
const HomeHeader = () => {
    const { state, dispatch } = useContext(AppContext);

    const menuref = useRef();
    const [isOpen, setIsOpen] = useState(false);
    const handleClick = (e) => {
        if (menuref.current.contains(e.target)) {
            return;
        }
        setIsOpen(false);
    };
    const openSidebar = () => {
        dispatch({
            type: 'TOOGLE_SIDEBAR'
        });
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClick);

        return () => {
            document.removeEventListener("mousedown", handleClick);
        };
    }, []);

    return (
        <HeaderContainer>
            <HeaderNav>
                <HeaderLeft>
                    <Logo src="/images/home-logo.svg" />
                </HeaderLeft>
                <NavBars onClick={() => openSidebar()}>
                    <Icon name='menu' />
                </NavBars>
                <HeaderRight>
                    <NavMenu>
                        <span>Home</span>
                    </NavMenu>
                    <NavMenu>
                        <span>My network</span>
                    </NavMenu>
                    <NavMenu>
                        <span>Jobs</span>
                    </NavMenu>
                    <NavMenu>
                        <span>Messaging</span>
                    </NavMenu>
                    <NavMenu>
                        <span>notifications</span>
                    </NavMenu>
                    <NavMenu>
                        <User ref={menuref} onClick={() => setIsOpen(!isOpen)}>
                            <Usertitle>
                                <span>Me</span>
                                <img src="/images/down-icon.svg" alt="me" />
                            </Usertitle>
                            {isOpen && (
                                <UserDropDown>
                                    <UserDetails>
                                        <Userdesc>
                                            <UserName>Sarang Ravikumar</UserName>
                                            <UseDescription>Student at toch institure</UseDescription>
                                        </Userdesc>
                                    </UserDetails>
                                    <button>view profile</button>
                                    <hr />
                                    <UserDropContent>
                                        <ContentHeading>Account</ContentHeading>
                                        <ContentList>Setting and privacy </ContentList>
                                        <ContentList>help</ContentList>
                                        <ContentList>language</ContentList>
                                        <hr />
                                        <ContentHeading>manage</ContentHeading>
                                        <ContentList>post and activity </ContentList>
                                        <ContentList>job posting account</ContentList>
                                    </UserDropContent>
                                </UserDropDown>
                            )}
                        </User>
                    </NavMenu>
                    <NavMenu>
                        <Work>
                            <Worktitle>
                                <Icon name='facebook' />
                                <img src="/images/down-icon.svg" alt="me" />
                            </Worktitle>
                        </Work>
                    </NavMenu>
                </HeaderRight>
            </HeaderNav>
        </HeaderContainer>
    );
};


export default HomeHeader;