import React from "react";
import styled from "styled-components";
import PropTypes from 'prop-types';

const SectionTitle = ({ title, color, size }) => {
  return (
    <TitleWrapper
      color={color}
      size={size}
      className="col text-center mb-5 letter-spacing"
    >
      <h2 className="text-uppercase mb-1">{title}</h2>
      <div className="title-underline" />
    </TitleWrapper>
  );
};

SectionTitle.propTypes = {
  title: PropTypes.string.isRequired,
  color: PropTypes.string,
  size: PropTypes.string
}
SectionTitle.defaultProps = {
  color: undefined,
  size: undefined,
};
export default SectionTitle;

const TitleWrapper = styled.div`
  /* color:var(--primaryColor); */
  color: ${props => (props.color ? props.color : "var(--primaryBlack)")};
  h2 {
    font-size: ${props => (props.size ? props.size : "2rem")};
  }
  .title-underline {
    width: 5rem;
    height: 0.25rem;
    margin: 0 auto;
    background: var(--primaryColor);
  }
`;