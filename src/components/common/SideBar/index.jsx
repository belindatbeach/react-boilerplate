import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AppContext from "@context/appContext";
import { SidebarWrapper } from './styles';
import Icon from "../Icon";
const Sidebar = ({layout}) => {
    const { state, dispatch } = useContext(AppContext);
    const { sidebar } = state;
    const closeSidebar = () => {
        dispatch({
            type: 'TOOGLE_SIDEBAR'
        });
    };
    const SiteBar = () => {
        return (
            <ul>
                <Link to={'/'} onClick={closeSidebar}>
                    <li className="link">
                        <p className="link-text">
                            <span>Security</span>
                        </p>
                    </li>
                    <li className="link">
                        <p className="link-text">
                            <span>Support</span>
                        </p>
                    </li>
                    <li className="link">
                        <p className="link-text">
                            <span>For Developer</span>
                        </p>
                    </li>
                </Link>
            </ul>
        )
    }
    
    return (
        <SidebarWrapper>
            <div className={`sidebar ${sidebar ? "active" : ""}`}>
                <div className="links-container">
                    <button className="close-btn" onClick={closeSidebar}>
                        <Icon name="close" />
                    </button>
                    {
                        SiteBar()
                    }
                </div>
            </div>
            <div
                className={`screen ${sidebar ? "active" : ""}`}
                onClick={closeSidebar}
            ></div>
        </SidebarWrapper>
    );
};


export default Sidebar;
