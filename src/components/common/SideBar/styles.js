import styled from 'styled-components';
export const SidebarWrapper = styled.aside`
.links-container {
}

.close-btn {
  font-size: 2.1rem;
  color: var(--primaryGrey);
  display: flex;
  align-self: center;
  background: none;
  border: none;
  margin-left: auto;
  padding: 1rem;
  :hover {
    color: var(--primaryWhite);
  }
  span{
    padding-right: 0px !important;
  }
}

.header {
  display: grid;
  place-items: center;
}

.underline {
  background-color: var(--primaryGrey);
  height: 1px;
  opacity: 0.8;
  margin-top: 0.4rem;
  width: 50%;
}

h3 {
  opacity: 0.9;
  font-weight: 300;
  text-transform: capitalize;
  color: var(--primaryWhite);
  padding: 0;
  margin: 0;
}
p {
  padding: 0;
  margin: 0;
}

ul {
  padding-top: 0.2rem;
  padding-bottom: 1.5rem;
}

.links-container span {
  color: var(--primaryGrey);
  font-size: 1.5rem;
  opacity: 0.9;
  display: flex;
  padding-right: 2.1rem;
  align-items: center;
}

.link {
  display: flex;
  align-items: center;
  padding: 0.8rem 2rem;
  transition: var(--transition);
}

.link-text {
  display: flex;
  font-size: 1.5rem;
  opacity: 0.9;
  letter-spacing: 1px;
  font-weight: 300;
  text-transform: capitalize;
  color: var(--primaryWhite);
  align-items: center;
  span {
      margin-left: 5px;
  }
}

.link:hover {
  padding-left: 3rem;
  background-color: var(--secondaryColor);
  .link-text {
    color: var(--primaryWhite);
  }
  span {
    color: var(--primaryWhite);
  }
}

.sidebar {
  width: 0%;
  position: fixed;
  min-height: 100%;
  z-index: 105;
  overflow-x: hidden;
  transition: var(--transition);
  background-color: var(--primaryBlack);
  right: 0;
  z-index: 9999999;
}

.screen {
  display: none;
  position: fixed;
  min-height: 100%;
  z-index: 104;
  min-width: 100vw;
  background-color: var(--primaryBlack);
  opacity: 0.6;
}

.active {
  width: 65vw;
  display: block;
}

@media screen and (min-width: 768px) {
  display: none;
}
`;