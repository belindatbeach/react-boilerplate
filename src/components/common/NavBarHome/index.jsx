import React, { useEffect, useState } from "react";
import { animateScroll as scroll, Element } from "react-scroll";
import Icon from "@components/common/Icon";
import { Nav, NavContainer, NavLogo, NavBars, NavMenu, NavLink, NavBtnLink, CloseNavMenu, } from "./styles";
import Button from "../Button";

const NavbarHome = () => {
  const [click, setClick] = useState(false);
  const [scrollNav, setScrollNav] = useState(false);
  const handleClick = () => {
    setClick(!click);
  };
  const handleScrollNav = () => {
    if (window.scrollY >= 80) {
      setScrollNav(true);
    } else {
      setScrollNav(false);
    }
  };
  useEffect(() => {
    window.addEventListener("scroll", handleScrollNav);
  }, []);
  const scrollTop = () => {
    scroll.scrollToTop();
  };
  return (
    <>
      <Element name="back-to-top" />
      <Nav scrollNav={scrollNav}>
        <NavContainer>
          <NavLogo to='/' onClick={scrollTop}>
            dolla
        </NavLogo>
          <NavBars onClick={handleClick}>
            <Icon name='menu' />
          </NavBars>
          <NavMenu click={click} onClick={handleClick}>
            <NavLink to='home' onClick={handleClick}>
              Home
          </NavLink>
            <NavLink to='about' onClick={handleClick}>
              About
            </NavLink>
            <NavLink to='services' onClick={handleClick}>
              Service
          </NavLink>
            <NavLink to='contact' onClick={handleClick}>
              Contact
          </NavLink>
            <Button type="secondary" width="110px">Log In</Button>
            <div className="px-1 py-1 py-md-0" />
            <Button width="110px">Sign Up</Button>
            <CloseNavMenu onClick={handleClick}>
              <Icon name='close' />
            </CloseNavMenu>
          </NavMenu>
        </NavContainer>
      </Nav>
    </>
  );
};

export default NavbarHome;