import React from 'react';
import PropTypes from 'prop-types';

import PageHead from '@components/common/PageHead';
import NavBar from '@components/common/NavBar';
import NavBarHome from '@components/common/NavBarHome';
import { Viewport } from './style';
import Header from '@components/common/Header';
import Sidebar from '@components/common/SideBar';

const Page = ({ children, showNav, layout, title, description }) => {
  return (
    <Viewport>
      <PageHead
        title={title}
        description={description}
      />
      <Sidebar />
      
      {layout == "account" ? <Header /> : <NavBarHome />}
      
      {showNav && (
        <NavBar />
      )}
       
      {children}
     
    </Viewport>
  );
}

Page.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element).isRequired,
    PropTypes.element.isRequired,
  ]).isRequired,
  showNav: PropTypes.bool,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  layout: PropTypes.string,
};


Page.defaultProps = {
  showNav: true,
  layout: null
};


export default Page;