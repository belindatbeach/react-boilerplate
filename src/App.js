import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css'; 
import "bootstrap/dist/css/bootstrap.min.css";
import GlobalStyles from '@components/GlobalStyles';
import { AppProvider } from '@context/appContext.js';
import Router from './router';

function App() {
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <>
      <GlobalStyles />
      <AppProvider>
        <Router />
      </AppProvider>
    </>
  );

}

export default App;
