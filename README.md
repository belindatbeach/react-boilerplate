# Boilerplate

## Tecnologias

- [ReactJS](https://reactjs.org/)
- [Axios](https://github.com/axios/axios)
- [Styled Components](https://styled-components.com/)
- [PropTypes](https://www.npmjs.com/package/prop-types)
- [React Hook Form](http://react-hook-form.com/)
- [ContextAPI](https://reactjs.org/docs/context.html)
- [React Hooks](https://reactjs.org/docs/hooks-intro.html)
- [Polished](https://polished.js.org/docs/)
- [React Helmet](https://www.npmjs.com/package/react-helmet)

